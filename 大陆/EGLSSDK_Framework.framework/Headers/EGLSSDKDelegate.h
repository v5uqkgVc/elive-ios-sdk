
#import <UIKit/UIKit.h>


@protocol EGLSSDKDelegate

@optional

/**
 SDK初始化后的返回 //0 成功  2 失败 
 */
- (void)eglsSdkInitCallBack:(NSString *) state andJsonVaule:(NSString *) json;

/**
 登录成功回调

 @param uid 用户ID
 @param token 校验用TOKEN
 @param channel 登录方式："0"表示游客，"1"表示EGLS，“2”表示google，“3”表示facebook, "4"表示微信登陆
 @param nick 用于显示的账号名称
 */
- (void)loginSuccessCallBackWithUid:(NSString *)uid andToken:(NSString *)token andChannel:(NSString *)channel andNickname:(NSString *)nick;

/**
 * @brief     fb登录回调
 * @param     uid    fb用户ID
 * @param     name    用户昵称
 picture 用户头像
 */
- (void)FBloginSuccessCallBackWithUid:(NSString *)uid name:(NSString *)name picture:(NSString *)picture;
/*
 FB登录回调
 fields 好友列表
*/
- (void)FBloginSuccessCallBackWithFields:(NSArray *)fields;

/*
 FB好友邀请回调
 friends 好友列表
 */
- (void)FBloginInviteCallBackWithFriends:(NSArray *)friends;


/**
 *    关掉登录面板的回调
 */
- (void)loginCancel;
/**
 * @brief     注册回调(免注册绑定，不算注册)
 * @param     uid    用户ID
 */
- (void)registerSuccessCallBackWithUid:(NSString *)uid;
/**
 *    苹果充值成功回调
 *
 *    @param    orderID    EGLS平台订单号
 *    @param    money    充值金额
 */
- (void)applePurchaseSuccessCallBackWithOrderID:(NSString *)orderID andMoney:(NSString *)money;
/**
 *    苹果充值失败回调
 *
 *    @param    orderID    EGLS平台订单号
 *    @param    money    充值金额
 */
- (void)applePurchaseFailCallBack:(NSString *)failState;


@optional
/**
 *    若接入Facebook分享，需实现以下delegate。——获取分享的结果。
 *
 */
- (void)facebookShare_DidCompleteWithResults:(NSDictionary *)results;

- (void)facebookShare_DidFailWithError:(NSError *)error;

- (void)facebookShare_DidCancel;

/**
 * 分享回调接口
 *
 */
- (void)shareCallBack:(int)typeCode withResult:(int)result withMessage:(NSString*)message;

/**
 运营活动回调接口

 @param typeCode type 0 = 五星好评 1 = facebook加入粉丝团 2 = facebook每日分享 3 = line分享
 @param isSuc 是否成功
 */
- (void)activityCallBack:(int)typeCode isSuccess:(BOOL)isSuc;

/**
 *    若需要登出功能，则实现此回调,第三方账号会直接登出，egls账号不变，弹出账号输入界面
 *
 */
- (void)eglsLogOut;


/**
 用户绑定回调

 @param accountType 绑定对应账户类型 1:EGLS 2:Google 3:Facebook 4:Wechat
 @param nickName 用户昵称
 */
- (void)channelBindCallBack:(NSString *)accountType withNickName:(NSString *)nickName withBindState: (NSString*) state;


/**
 登录失败回调

 @param channel 登录方式："0"表示游客，"1"表示EGLS，“2”表示google，“3”表示facebook, "4"表示微信登陆
 */
- (void)loginFailCallBackWithChannel:(NSString *)channel;


/**
 点击协议协议回调函数

 @param state 点击状态 0:点击取消 1:点击同意
 */
-(void)touchAgreementCallBack:(BOOL)state;


@end

@protocol EGLSSDKDelegatePadding
@end
