
/**
 登陆方式
 
 - MODE_LOGIN_COMMON: 普通登陆方式，对应原接口showLogin
 - MODE_LOGIN_AUTO: 自动登陆方式，对应原接口autoLogin
 - MODE_LOGIN_FAST: 快速登陆方式，在自动登陆基础上，如果玩家第一次登陆游戏，不会弹出sdk页面，直接以游客方式登陆游戏
 -MODE_LOGIN_LIGHTLY:轻量级登录
 */
typedef NS_ENUM(NSInteger, CPLoginType) {
    MODE_LOGIN_COMMON,
    MODE_LOGIN_AUTO,
    MODE_LOGIN_FAST,
    MODE_LOGIN_LIGHTLY
};

@interface EGLSSDKLoginManager : NSObject
+ (EGLSSDKLoginManager *)sharedInstance;

/**
 * @brief     自动登录
 * @description
  若之前登录过，本地存了账号密码，那么会自动登录，不弹出sdk面板；
  若没登录过，弹出登录面板。
 * @return    无返回
 */
- (void)autoLogin;

/**
 * @brief     登录，必定会弹出登录界面
 * @description   可用于切换账号
 * @return    无返回
 */
- (void)showLogin;

//快速登录
-(void)fastLogin;

@end
